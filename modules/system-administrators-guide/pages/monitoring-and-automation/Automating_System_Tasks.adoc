
:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Automating_System_Tasks]]
= Automating System Tasks
indexterm:[Automated Tasks]
Tasks, also known as _jobs_, can be configured to run automatically within a specified period of time, on a specified date, or when the system load average decreases below 0.8.

{MAJOROS} is pre-configured to run important system tasks to keep the system updated. For example, the slocate database used by the [command]#locate# command is updated daily. A system administrator can use automated tasks to perform periodic backups, monitor the system, run custom scripts, and so on.

{MAJOROS} comes with the following automated task utilities: [command]#cron#, [command]#anacron#, [command]#at#, and [command]#batch#.

Every utility is intended for scheduling a different job type: while Cron and Anacron schedule recurring jobs, At and Batch schedule one-time jobs (refer to xref:Automating_System_Tasks.adoc#s1-autotasks-cron-anacron[Cron and Anacron] and xref:Automating_System_Tasks.adoc#s1-autotasks-at-batch[At and Batch] respectively).

{MAJOROS} supports the use of `systemd.timer` for executing a job at a specific time. See man `systemd.timer(5)` for more information.

[[s1-autotasks-cron-anacron]]
== Cron and Anacron
indexterm:[anacron]indexterm:[cron]
Both Cron and Anacron can schedule execution of recurring tasks to a certain point in time defined by the exact time, day of the month, month, day of the week, and week.

Cron jobs can run as often as every minute. However, the utility assumes that the system is running continuously and if the system is not on at the time when a job is scheduled, the job is not executed.

On the other hand, Anacron remembers the scheduled jobs if the system is not running at the time when the job is scheduled. The job is then executed as soon as the system is up. However, Anacron can only run a job once a day. Also note that by default, Anacron only runs when your system is running on AC power and will not run if your system is being powered by a battery; this behavior is set up in the [filename]`/etc/cron.hourly/0anacron` script.

[[sect-Cron-Installing]]
=== Installing Cron and Anacron

To install Cron and Anacron, you need to install the [package]*cronie* package with Cron and the [package]*cronie-anacron* package with Anacron ([package]*cronie-anacron* is a sub-package of [package]*cronie*).

To determine if the packages are already installed on your system, issue the following command:

[subs="quotes, macros"]
----
[command]#rpm -q cronie cronie-anacron#
----

The command returns full names of the [package]*cronie* and [package]*cronie-anacron* packages if already installed, or notifies you that the packages are not available.

To install these packages, use the [command]#dnf# command in the following form as `root`:

[subs="quotes, macros"]
----
[command]#dnf install _package_pass:attributes[{blank}]#
----

For example, to install both Cron and Anacron, type the following at a shell prompt:

[subs="attributes"]
----
~]#{nbsp}dnf install cronie cronie-anacron
----

For more information on how to install new packages in {MAJOROS}, see xref:package-management/DNF.adoc#sec-Installing[Installing Packages].

[[sect-Cron-Running]]
=== Running the Crond Service

The cron and anacron jobs are both picked by the `crond` service. This section provides information on how to start, stop, and restart the `crond` service, and shows how to configure it to start automatically at boot time.

[[sect-Cron-service]]
==== Starting and Stopping the Cron Service

To determine if the service is running, use the following command:

[subs="quotes, macros"]
----
[command]#systemctl status crond.service#
----

To run the `crond` service in the current session, type the following at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#systemctl start crond.service#
----

To configure the service to start automatically at boot time, use the following command as `root`:

[subs="quotes, macros"]
----
[command]#systemctl enable crond.service#
----

[[sect-Crond_Stopping]]
==== Stopping the Cron Service

To stop the `crond` service in the current session, type the following at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#systemctl stop crond.service#
----

To prevent the service from starting automatically at boot time, use the following command as `root`:

[subs="quotes, macros"]
----
[command]#systemctl disable crond.service#
----

[[sect-Crond_Restarting]]
==== Restarting the Cron Service

To restart the `crond` service, type the following at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#systemctl restart crond.service#
----

This command stops the service and starts it again in quick succession.

[[s2-configuring-anacron-jobs]]
=== Configuring Anacron Jobs
indexterm:[anacron,anacron configuration file]indexterm:[anacrontab]indexterm:[anacron,user-defined tasks]indexterm:[/var/spool/anacron]
The main configuration file to schedule jobs is the `/etc/anacrontab` file, which can be only accessed by the `root` user. The file contains the following:

----
SHELL=/bin/sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root
# the maximal random delay added to the base delay of the jobs
RANDOM_DELAY=45
# the jobs will be started during the following hours only
START_HOURS_RANGE=3-22

#period in days   delay in minutes   job-identifier   command
1         5     cron.daily    nice run-parts /etc/cron.daily
7         25    cron.weekly   nice run-parts /etc/cron.weekly
@monthly  45    cron.monthly  nice run-parts /etc/cron.monthly
----

The first three lines define the variables that configure the environment in which the anacron tasks run:

* `SHELL` &mdash; shell environment used for running jobs (in the example, the Bash shell)

* `PATH` &mdash; paths to executable programs

* `MAILTO` &mdash; username of the user who receives the output of the anacron jobs by email
+
If the `MAILTO` variable is not defined (`MAILTO=`), the email is not sent.

The next two variables modify the scheduled time for the defined jobs:

* `RANDOM_DELAY` &mdash; maximum number of minutes that will be added to the `delay in minutes` variable which is specified for each job
+
The minimum delay value is set, by default, to 6 minutes.
+
If `RANDOM_DELAY` is, for example, set to `12`, then between 6 and 12 minutes are added to the `delay in minutes` for each job in that particular anacrontab. `RANDOM_DELAY` can also be set to a value below `6`, including `0`. When set to `0`, no random delay is added. This proves to be useful when, for example, more computers that share one network connection need to download the same data every day.

* `START_HOURS_RANGE` &mdash; interval, when scheduled jobs can be run, in hours
+
In case the time interval is missed, for example due to a power failure, the scheduled jobs are not executed that day.

The remaining lines in the `/etc/anacrontab` file represent scheduled jobs and follow this format:

[subs="quotes"]
----

period in days   delay in minutes   job-identifier   command

----

* `period in days` &mdash; frequency of job execution in days
+
The property value can be defined as an integer or a macro (`@daily`, `@weekly`, `@monthly`), where `@daily` denotes the same value as integer 1, `@weekly` the same as 7, and `@monthly` specifies that the job is run once a month regardless of the length of the month.

* `delay in minutes` &mdash; number of minutes anacron waits before executing the job
+
The property value is defined as an integer. If the value is set to `0`, no delay applies.

* `job-identifier` &mdash; unique name referring to a particular job used in the log files

* `command` &mdash; command to be executed
+
The command can be either a command such as [command]#ls /proc >> /tmp/proc# or a command which executes a custom script.

Any lines that begin with a hash sign (#) are comments and are not processed.

[[s3-anacron-examples]]
==== Examples of Anacron Jobs

The following example shows a simple `/etc/anacrontab` file:

----
SHELL=/bin/sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root

# the maximal random delay added to the base delay of the jobs
RANDOM_DELAY=30
# the jobs will be started during the following hours only
START_HOURS_RANGE=16-20

#period in days   delay in minutes   job-identifier   command
1         20    dailyjob      nice run-parts /etc/cron.daily
7         25    weeklyjob     /etc/weeklyjob.bash
@monthly  45    monthlyjob    ls /proc >> /tmp/proc
----

All jobs defined in this `anacrontab` file are randomly delayed by 6-30 minutes and can be executed between 16:00 and 20:00.

The first defined job is triggered daily between 16:26 and 16:50 (RANDOM_DELAY is between 6 and 30 minutes; the `delay in minutes` property adds 20 minutes). The command specified for this job executes all present programs in the `/etc/cron.daily/` directory using the [command]#run-parts# script (the [command]#run-parts# scripts accepts a directory as a command-line argument and sequentially executes every program in the directory). See the `run-parts` man page for more information on the [command]#run-parts# script.

The second job executes the `weeklyjob.bash` script in the `/etc/` directory once a week.

The third job runs a command, which writes the contents of `/proc` to the `/tmp/proc` file ([command]#ls /proc >> /tmp/proc#) once a month.

[[s2-configuring-cron-jobs]]
=== Configuring Cron Jobs
indexterm:[cron,user-defined tasks]indexterm:[/var/spool/cron]indexterm:[cron,cron configuration file]indexterm:[crontab]
The configuration file for cron jobs is `/etc/crontab`, which can be only modified by the `root` user. The file contains the following:

----
SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=root
HOME=/
# For details see man 4 crontabs
# Example of job definition:
# .---------------- minute (0 - 59)
# | .------------- hour (0 - 23)
# | | .---------- day of month (1 - 31)
# | | | .------- month (1 - 12) OR jan,feb,mar,apr ...
# | | | | .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# | | | | |
# * * * * * user-name command to be executed
----

The first three lines contain the same variable definitions as an `anacrontab` file: `SHELL`, `PATH`, and `MAILTO`. For more information about these variables, see xref:Automating_System_Tasks.adoc#s2-configuring-anacron-jobs[Configuring Anacron Jobs].

In addition, the file can define the `HOME` variable. The `HOME` variable defines the directory, which will be used as the home directory when executing commands or scripts run by the job.

The remaining lines in the `/etc/crontab` file represent scheduled jobs and have the following format:

[subs="quotes"]
----
minute   hour   day   month   day of week   username   command
----

The following define the time when the job is to be run:

* `minute` &mdash; any integer from 0 to 59

* `hour` &mdash; any integer from 0 to 23

* `day` &mdash; any integer from 1 to 31 (must be a valid day if a month is specified)

* `month` &mdash; any integer from 1 to 12 (or the short name of the month such as jan or feb)

* `day of week` &mdash; any integer from 0 to 7, where 0 or 7 represents Sunday (or the short name of the week such as sun or mon)

The following define other job properties:

* `username` &mdash; specifies the user under which the jobs are run.

* `command` &mdash; the command to be executed.
+
The command can be either a command such as [command]#ls /proc /tmp/proc# or a command which executes a custom script.

For any of the above values, an asterisk (*) can be used to specify all valid values. If you, for example, define the month value as an asterisk, the job will be executed every month within the constraints of the other values.

A hyphen (-) between integers specifies a range of integers. For example, `1-4` means the integers 1, 2, 3, and 4.

A list of values separated by commas (,) specifies a list. For example, `3,4,6,8` indicates exactly these four integers.

The forward slash (/) can be used to specify step values. The value of an integer will be skipped within a range following the range with `/pass:attributes[{blank}]_integer_pass:attributes[{blank}]`. For example, the minute value defined as `0-59/2` denotes every other minute in the minute field. Step values can also be used with an asterisk. For instance, if the month value is defined as `*/3`, the task will run every third month.

Any lines that begin with a hash sign (#) are comments and are not processed.

Users other than `root` can configure cron tasks with the [command]#crontab# utility. The user-defined crontabs are stored in the `/var/spool/cron/` directory and executed as if run by the users that created them.

To create a crontab as a specific user, login as that user and type the command [command]#crontab -e# to edit the user's crontab with the editor specified in the `VISUAL` or `EDITOR` environment variable. The file uses the same format as `/etc/crontab`. When the changes to the crontab are saved, the crontab is stored according to the user name and written to the file `/var/spool/cron/pass:attributes[{blank}]_username_pass:attributes[{blank}]`. To list the contents of the current user's crontab file, use the [command]#crontab -l# command.

The `/etc/cron.d/` directory contains files that have the same syntax as the `/etc/crontab` file. Only `root` is allowed to create and modify files in this directory.

.Do not restart the daemon to apply the changes
[NOTE]
====

The cron daemon checks the `/etc/anacrontab` file, the `/etc/crontab` file, the `/etc/cron.d/` directory, and the `/var/spool/cron/` directory every minute for changes and the detected changes are loaded into memory. It is therefore not necessary to restart the daemon after an anacrontab or a crontab file have been changed.

====

[[s2-autotasks-cron-access]]
=== Controlling Access to Cron

To restrict the access to Cron, you can use the `/etc/cron.allow` and `/etc/cron.deny` files. These access control files use the same format with one user name on each line. Mind that no whitespace characters are permitted in either file.

If the `cron.allow` file exists, only users listed in the file are allowed to use cron, and the `cron.deny` file is ignored.

If the `cron.allow` file does not exist, users listed in the `cron.deny` file are not allowed to use Cron.

The Cron daemon (`crond`) does not have to be restarted if the access control files are modified. The access control files are checked each time a user tries to add or delete a cron job.

The `root` user can always use cron, regardless of the user names listed in the access control files.

You can control the access also through Pluggable Authentication Modules (PAM). The settings are stored in the `/etc/security/access.conf` file. For example, after adding the following line to the file, no other user but the `root` user can create crontabs:

[subs="quotes"]
----
-:ALL EXCEPT root :cron
----

The forbidden jobs are logged in an appropriate log file or, when using [command]#crontab -e#, returned to the standard output. For more information, see the `access.conf.5` manual page.

[[s2-black-white-listing-of-cron-jobs]]
=== Black and White Listing of Cron Jobs

Black and white listing of jobs is used to define parts of a job that do not need to be executed. This is useful when calling the [application]*run-parts* script on a Cron directory, such as `/etc/cron.daily/`: if the user adds programs located in the directory to the job black list, the [application]*run-parts* script will not execute these programs.

To define a black list, create a `jobs.deny` file in the directory that [command]#run-parts# scripts will be executing from. For example, if you need to omit a particular program from `/etc/cron.daily/`, create the `/etc/cron.daily/jobs.deny` file. In this file, specify the names of the programs to be omitted from execution (only programs located in the same directory can be enlisted). If a job runs a command which runs the programs from the `/etc/cron.daily/` directory, such as [command]#run-parts /etc/cron.daily#, the programs defined in the `jobs.deny` file will not be executed.

To define a white list, create a `jobs.allow` file.

The principles of `jobs.deny` and `jobs.allow` are the same as those of `cron.deny` and `cron.allow` described in section xref:Automating_System_Tasks.adoc#s2-autotasks-cron-access[Controlling Access to Cron].

[[s1-autotasks-at-batch]]
== At and Batch
indexterm:[at]indexterm:[batch]
While Cron is used to schedule recurring tasks, the [application]*At* utility is used to schedule a one-time task at a specific time and the [application]*Batch* utility is used to schedule a one-time task to be executed when the system load average drops below 0.8.

[[sect-At_and_Batch_Installation]]
=== Installing At and Batch

To determine if the [package]*at* package is already installed on your system, issue the following command:

[subs="quotes, macros"]
----
[command]#rpm -q at#
----

The command returns the full name of the [package]*at* package if already installed or notifies you that the package is not available.

To install the packages, use the [command]#dnf# command in the following form as `root`:

[subs="quotes, macros"]
----
[command]#dnf install _package_pass:attributes[{blank}]#
----

For example, to install both At and Batch, type the following at a shell prompt:

[subs="attributes"]
----
~]#{nbsp}dnf install at
----

For more information on how to install new packages in {MAJOROS}, see xref:package-management/DNF.adoc#sec-Installing[Installing Packages].

[[sect-Atd-Running]]
=== Running the At Service

The At and Batch jobs are both picked by the `atd` service. This section provides information on how to start, stop, and restart the `atd` service, and shows how to configure it to start automatically at boot time.

[[sect-Atd-service]]
==== Starting and Stopping the At Service

To determine if the service is running, use the following command:

[subs="quotes, macros"]
----
[command]#systemctl status atd.service#
----

To run the `atd` service in the current session, type the following at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#systemctl start atd.service#
----

To configure the service to start automatically at boot time, use the following command as `root`:

[subs="quotes, macros"]
----
[command]#systemctl enable atd.service#
----

[NOTE]
====

It is recommended that you configure your system to start the `atd` service automatically at boot time.

====

[[sect-Atd_Stopping]]
==== Stopping the At Service

To stop the `atd` service, type the following at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#systemctl stop atd.service#
----

To prevent the service from starting automatically at boot time, use the following command as `root`:

[subs="quotes, macros"]
----
[command]#systemctl disable atd.service#
----

[[sect-Atd_Restarting]]
==== Restarting the At Service

To restart the `atd` service, type the following at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#systemctl restart atd.service#
----

This command stops the service and starts it again in quick succession.

[[s2-autotasks-at-configuring]]
=== Configuring an At Job

To schedule a one-time job for a specific time with the [application]*At* utility, do the following:

. On the command line, type the command [command]#at _TIME_pass:attributes[{blank}]#, where [command]#pass:attributes[{blank}]_TIME_pass:attributes[{blank}]# is the time when the command is to be executed.
+
The _TIME_ argument can be defined in any of the following formats:
+
** `pass:attributes[{blank}]_HH_:pass:attributes[{blank}]_MM_pass:attributes[{blank}]` specifies the exact hour and minute; For example, `04:00` specifies 4:00 a.m.
+
** `midnight` specifies 12:00 a.m.
+
** `noon` specifies 12:00 p.m.
+
** `teatime` specifies 4:00 p.m.
+
** `pass:attributes[{blank}]_MONTH_pass:attributes[{blank}]pass:attributes[{blank}]_DAY_pass:attributes[{blank}]pass:attributes[{blank}]_YEAR_pass:attributes[{blank}]` format; For example, `January 15 2012` specifies the 15th day of January in the year 2012. The year value is optional.
+
** `pass:attributes[{blank}]_MMDDYY_pass:attributes[{blank}]`, `pass:attributes[{blank}]_MM_pass:attributes[{blank}]/pass:attributes[{blank}]_DD_pass:attributes[{blank}]/pass:attributes[{blank}]_YY_pass:attributes[{blank}]`, or `pass:attributes[{blank}]_MM_._DD_._YY_pass:attributes[{blank}]` formats; For example, `011512` for the 15th day of January in the year 2012.
+
** `now + _TIME_pass:attributes[{blank}]` where _TIME_ is defined as an integer and the value type: minutes, hours, days, or weeks. For example, `now + 5 days` specifies that the command will be executed at the same time five days from now.
+
The time must be specified first, followed by the optional date. For more information about the time format, see the `/usr/share/doc/at-_<version>_pass:attributes[{blank}]/timespec` text file.
+
If the specified time has past, the job is executed at the time the next day.

. In the displayed `at>` prompt, define the job commands:
+
.. Type the command the job should execute and press kbd:[Enter]. Optionally, repeat the step to provide multiple commands.
+
.. Enter a shell script at the prompt and press kbd:[Enter] after each line in the script.
+
The job will use the shell set in the user's `SHELL` environment, the user's login shell, or [command]#/bin/sh# (whichever is found first).

. Once finished, press kbd:[Ctrl + D] on an empty line to exit the prompt.

If the set of commands or the script tries to display information to standard output, the output is emailed to the user.

To view the list of pending jobs, use the [command]#atq# command. See xref:Automating_System_Tasks.adoc#s2-autotasks-at-batch-viewing[Viewing Pending Jobs] for more information.

You can also restrict the usage of the [command]#at# command. For more information, see xref:Automating_System_Tasks.adoc#s2-autotasks-at-batch-controlling-access[Controlling Access to At and Batch] for details.

[[s2-autotasks-batch-configuring]]
=== Configuring a Batch Job

The [application]*Batch* application executes the defined one-time tasks when the system load average decreases below 0.8.

To define a Batch job, do the following:

. On the command line, type the command [command]#batch#.

. In the displayed `at>` prompt, define the job commands:
+
.. Type the command the job should execute and press kbd:[Enter]. Optionally, repeat the step to provide multiple commands.
+
.. Enter a shell script at the prompt and press kbd:[Enter] after each line in the script.
+
If a script is entered, the job uses the shell set in the user's `SHELL` environment, the user's login shell, or [command]#/bin/sh# (whichever is found first).

. Once finished, press kbd:[Ctrl + D] on an empty line to exit the prompt.

If the set of commands or the script tries to display information to standard output, the output is emailed to the user.

To view the list of pending jobs, use the [command]#atq# command. See xref:Automating_System_Tasks.adoc#s2-autotasks-at-batch-viewing[Viewing Pending Jobs] for more information.

You can also restrict the usage of the [command]#batch# command. For more information, see xref:Automating_System_Tasks.adoc#s2-autotasks-at-batch-controlling-access[Controlling Access to At and Batch] for details.

[[s2-autotasks-at-batch-viewing]]
=== Viewing Pending Jobs

To view the pending [command]#At# and [command]#Batch# jobs, run the [command]#atq# command. The [command]#atq# command displays a list of pending jobs, with each job on a separate line. Each line follows the job number, date, hour, job class, and user name format. Users can only view their own jobs. If the `root` user executes the [command]#atq# command, all jobs for all users are displayed.

[[s2-autotasks-commandline-options]]
=== Additional Command Line Options

Additional command line options for [command]#at# and [command]#batch# include the following:

[[tb-at-command-line-options]]
.[command]#at# and [command]#batch# Command Line Options

[options="header"]
|===
|Option|Description
|[option]`-f`|Read the commands or shell script from a file instead of specifying them at the prompt.
|[option]`-m`|Send email to the user when the job has been completed.
|[option]`-v`|Display the time that the job is executed.
|===

[[s2-autotasks-at-batch-controlling-access]]
=== Controlling Access to At and Batch

You can restrict the access to the [command]#at# and [command]#batch# commands using the `/etc/at.allow` and `/etc/at.deny` files. These access control files use the same format defining one user name on each line. Mind that no whitespace are permitted in either file.

If the file `at.allow` exists, only users listed in the file are allowed to use [command]#at# or [command]#batch#, and the `at.deny` file is ignored.

If `at.allow` does not exist, users listed in `at.deny` are not allowed to use [command]#at# or [command]#batch#.

The [command]#at# daemon ([command]#atd#) does not have to be restarted if the access control files are modified. The access control files are read each time a user tries to execute the [command]#at# or [command]#batch# commands.

The `root` user can always execute [command]#at# and [command]#batch# commands, regardless of the content of the access control files.

[[s1-autotasks-additional-resources]]
== Additional Resources
indexterm:[cron,additional resources]indexterm:[at,additional resources]indexterm:[batch,additional resources]
To learn more about configuring automated tasks, see the following installed documentation:

* `cron(8)` man page contains an overview of cron.

* `crontab` man pages in sections 1 and 5:
+
** The manual page in section 1 contains an overview of the `crontab` file.
+
** The man page in section 5 contains the format for the file and some example entries.

* `anacron(8)` manual page contains an overview of anacron.

* `anacrontab(5)` manual page contains an overview of the `anacrontab` file.

* `run-parts(4)` manual page contains an overview of the [command]#run-parts# script.

* `/usr/share/doc/at/timespec` contains detailed information about the time values that can be used in cron job definitions.

* `at` manual page contains descriptions of [command]#at# and [command]#batch# and their command line options.
